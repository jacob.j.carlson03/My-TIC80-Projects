
SCREEN_W=240
SCREEN_H=136
r=15
cX = SCREEN_W*0.75
cY = SCREEN_H*0.5
oX = SCREEN_W*0.25
oY = SCREEN_H*0.5

function distForm(x1,y1,x2,y2)
	return math.sqrt(((x2-x1)*(x2-x1))+((y1-y2)*(y1-y2)))
end


function TIC()
	cls(0)
	r = 15*math.sin(time()/2000)+20
	--cX = 20*math.sin(time()/2000)+SCREEN_W*0.75
	--for x=0, SCREEN_W do
	--	for y=0, SCREEN_H do
	for x=cX-35, cX+35 do
		for y=cY-35, cY+35 do
			dist = distForm(cX,cY,x,y)
			if dist < r then
				pix(x,y,15)
			elseif dist < r+0.3 then--and dist > r-0.3 then
				pix(x,y, 2)
			end
		end
	end
	--circ(oX,oY,r+0.3,2)
	circ(oX,oY,r,15)
	--rect(cX-r-2,cX+r+2,cY-r-2,cY+r+2,14)
end
