-- title:  Rain
-- author: Jacob
-- desc:   Rainy!
-- script: lua

SCREEN_W=240
SCREEN_H=136
startTransition=false
bgColor=15
rainColor=3
rainSpeedChange=5000
buffer={}
ofs=0
for x=0,SCREEN_W do 
	buffer[x]={}
	for y=0,SCREEN_H do
		buffer[x][y]=pix(x,y)
	end
end

function drawConsole(offset)
		for x=0,SCREEN_W do
			for y=0,SCREEN_H do
				pix(x,y-offset,buffer[x][y])
			end
		end
end

poke(0x3FC0, 0x0001)
function TIC()
	if startTransition then
		drawConsole(ofs)
		ofs=ofs+1
	else
		sfx(0,math.floor(6*math.sin(time()/rainSpeedChange)+88),2,0,2.5*math.sin(time()/rainSpeedChange)+8,0)
		cls(15)
		map()
		for i=0,SCREEN_W do
			if math.random()>0.1*-math.sin(time()/rainSpeedChange)+0.88 then
				line(i,0,i+3*math.sin(time()/rainSpeedChange)+7,SCREEN_H+3*math.random()-1,rainColor)
			end
			if math.random()>0.03*-math.sin(time()/rainSpeedChange)+0.96 then
				line(i,0,i+3*math.sin(time()/rainSpeedChange)+7,SCREEN_H+3*math.random()-1,2)
			end
		end
		if math.random()>0.999 then sfx(2,3,60,1,15,0) end
	end
end