-- title:  
-- author: Jacob
-- desc:   
-- script: lua

SCREEN_W=240
SCREEN_WD2=120
SCREEN_H=136
SCREEN_HD2=68
drawY=0 --the generation
decimalRule=57
--
--function toBits(num)
--    -- returns a table of bits, least significant first.
--    local t={} -- will contain the bits
--    while num>0 do
--        rest=math.fmod(num,2)
--        t[#t+1]=rest
--        num=(num-rest)/2
--    end
--    return t
--end
--
rule={0,0,0,0,0,0,0}

function makeRule()
    local r = decimalRule
    for i=8,1,-1 do
        rule[i]=r & 1
        r=r>>1
    end
end
makeRule()

nextGen={}

function rules(l,m,r)    
    return rule[8-(l*4+m*2+r)]                             
end

function update()

    for i=0,SCREEN_W do
        left = pix(i-1,drawY)
        me = pix(i,drawY)
        right = pix(i+1,drawY)
        nextGen[i]= rules(left,me,right)
    end

    drawY=drawY+1
    
    for i=0,SCREEN_W do
        pix(i,drawY,nextGen[i])
    end
end

poke(0x03FF8, 1)
cls(0)
pix(SCREEN_WD2,drawY,1)

function reset()
    cls(0)
    drawY=0
    pix(SCREEN_WD2,drawY,1)
end

function TIC()
    if not btn(4) then
        if btnp(0,60,2) and decimalRule<256 then 
            decimalRule=decimalRule+1
            makeRule()
            reset()
        elseif btnp(1,60,2) and decimalRule>0 then 
            decimalRule=decimalRule-1
            makeRule()
            reset()
        end
        
        if drawY <= SCREEN_H-7 then 
            update()
        else 
            poke(0x03FF8, 1)--math.floor(math.random()*16))
            for i=0,SCREEN_W do
                pix(i,0,pix(i,SCREEN_H-8)) 
            end
            rect(0,2,SCREEN_W,SCREEN_H-7,0)
            drawY=0 
        end
        print("A: Pause",SCREEN_W-42,SCREEN_H-5,1)
        print("Up/Down: Change Rule",SCREEN_WD2-56,SCREEN_H-5,1)
        print("Rule:"..decimalRule,5,SCREEN_H-5,1)
    end
end