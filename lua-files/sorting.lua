-- title:  game title
-- author: game developer
-- desc:   short description
-- script: lua

SCREEN_W=240
SCREEN_H=136
NUM = 32
barArray = {}
t = 0
delay = 15
for i=1,NUM do
	barArray[i]=i
end
randomBar = tonumber(barArray[math.random(NUM)])
ordered = false
orderedFirstRun = true
doneCounter = 1
allDone = false
tAdd = 1

function shuffle(tbl)
  size = #tbl
  for i = size, 1, -1 do
    local rand = math.random(size)
    tbl[i], tbl[rand] = tbl[rand], tbl[i]
  end
  return tbl
end

function compareAndMove()
	for i=chosenBar,NUM-1 do
			if barArray[i] > chosenBar then
				if barArray[i] > barArray[i+1] then
					barArray[i], barArray[i+1] = barArray[i+1], barArray[i]
				end 
			end
		end
	for i=2,chosenBar-1 do
			if barArray[i] < chosenBar then
				if barArray[i] < barArray[i-1] then
					barArray[i], barArray[i-1] = barArray[i-1], barArray[i]
				end 
			end
		end
end

function drawBars(barColor, chosenBarColor)
	for i=1,NUM do
			if barArray[i] == chosenBar then
				rect(SCREEN_W/NUM*i,SCREEN_H-barArray[i]*(SCREEN_H/NUM),4,SCREEN_H,chosenBarColor)
			else 
				rect(SCREEN_W/NUM*i,SCREEN_H-barArray[i]*(SCREEN_H/NUM),4,SCREEN_H,barColor)
			end
		end
end

shuffle(barArray)
function TIC()
	if ordered then tAdd = delay/2 end
	t = t + tAdd
	if t%delay==0 and (not allDone) then
		cls(0)

		for i=1,NUM-1 do
			if i == barArray[i] then
				ordered = true
			else 
				ordered = false
				break
			end
		end

		
		if not ordered then
			chosenBar = tonumber(barArray[math.random(NUM)]) 
			compareAndMove()
		else
			if orderedFirstRun then chosenBar = 0; orderedFirstRun = false end
			chosenBar = chosenBar + 1
			if chosenBar == NUM then allDone = true end
		end
		sfx(0,chosenBar*2,10,0,6,0)
		--print(chosenBar)
		drawBars(15,6)
	end
end